import qbs

QtApplication {
    Depends { name: "Qt.widgets" }

    cpp.cxxLanguageVersion: "c++11"

    cpp.defines: [
        // You can make your code fail to compile if it uses deprecated APIs.
        // In order to do so, uncomment the following line.
        //"QT_DISABLE_DEPRECATED_BEFORE=0x060000" // disables all the APIs deprecated before Qt 6.0.0
    ]

    files: [
        "main.cpp"
    ]

    Group {
        name: "gui"
        prefix: "gui/"
        files: [
            "*.h",
            "*.cpp",
        ]
    }

    Group {
        name: "AgilentE364XA"
        prefix: "AgilentE364XA/"
        files: [
            "*.h",
            "*.cpp",
        ]
    }

    Group {     // Properties for the produced executable
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: "bin"
    }
}
